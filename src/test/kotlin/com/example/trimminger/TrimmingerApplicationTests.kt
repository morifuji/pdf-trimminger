package com.example.trimminger

import org.apache.pdfbox.pdmodel.PDDocument
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Primary
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.mock.web.MockMultipartFile
import org.springframework.stereotype.Service
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

/**
 * link: https://stackoverflow.com/questions/59230041/argumentmatchers-any-must-not-be-null
 */
private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)
object MockitoHelper {
    fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    @Suppress("UNCHECKED_CAST")
    fun <T> uninitialized(): T = null as T
}

@Service
@Primary
class MockPdfService : PdfService {
    override fun crop(file: InputStream, paddingLeft: Int, paddingTop: Int, paddingRight: Int, paddingBottom: Int): PDDocument {
        return PDDocument.load(File("src/test/dummy.pdf"))
    }

    override fun echo(): String {
        return "EEEOO from Mock!"
    }
}

@SpringBootTest
@AutoConfigureMockMvc
class TrimmingerApplicationTests(
        @Autowired val controller: HomeController,
        @Autowired val mockMvc: MockMvc,
        @Autowired val service: PdfService) {

//    @MockBean
//    private val service: PdfService? = null

    @Test
    fun contextLoads() {

//        `when`(service.crop(
//                ArgumentMatchers.any(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt())
//        ).thenReturn(PDDocument.load(File("src/test/dummy.pdf")))

        assertThat(controller).isNotNull()

        File("src/test/out.pdf").createNewFile()

        val pdf = File("src/test/test.pdf")
        val file = MockMultipartFile(
                "file",
                "test_contract.pdf",
                MediaType.APPLICATION_PDF_VALUE,
                pdf.readBytes())
        val res: ResponseEntity<StreamingResponseBody> = controller.cropPdf(file, 0, 40, 60, 100)

        assert(res.statusCode.is2xxSuccessful)
        assert(res.headers.contains("Content-Type"))
        assertThat(res.headers["Content-Type"]).isEqualTo(listOf("application/pdf"))
    }

    @Test
    fun checkResponse() {
//        `when`(service!!.crop(
//                MockitoHelper.anyObject(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt(),
//                ArgumentMatchers.anyInt())
//        ).thenReturn(PDDocument.load(File("src/test/dummy.pdf")))

        val pdf = File("src/test/test.pdf")
        val file = MockMultipartFile(
                "upload_file",
                "test_contract.pdf",
                MediaType.APPLICATION_PDF_VALUE,
                pdf.readBytes())
        val params = LinkedMultiValueMap<String, String>()
        params.add("padding_top", "10")
        params.add("padding_right", "20")
        params.add("padding_bottom", "30")
        params.add("padding_left", "40")

        MockMvcRequestBuilders.multipart("/api/trim")
                .file(file)
                .params(params)

//        if (mockMvc == null) {
//            fail("mockMVC is not exist!")
//        }

        mockMvc.perform(
                MockMvcRequestBuilders.multipart("/api/trim").file(file).params(params)
        ).andDo(
                // too long time
                print()
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_PDF))
    }

    @Test
    fun test1() {
        val myAge = 26
        val fatherAge = 56
        val diff = fatherAge - myAge

        assertThat(diff).isEqualTo(30)
    }
}

