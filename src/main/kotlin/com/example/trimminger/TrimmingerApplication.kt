package com.example.trimminger


import org.apache.pdfbox.pdmodel.PDDocument
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.OutputStream
import java.util.*


@SpringBootApplication
class TrimmingerApplication

fun main(args: Array<String>) {
    runApplication<TrimmingerApplication>(*args)
}

@CrossOrigin(origins = ["http://localhost:3001/", "*"])
@Controller
class HomeController(val pdfService: PdfService) {

    @RequestMapping("/")
    protected fun redirect(): String? {
        return "forward:/index.html"
    }

    @PostMapping("/api/trim")
    @ResponseBody
    fun cropPdf(
            @RequestParam("upload_file") multipartFile: MultipartFile,
            @RequestParam("padding_top") paddingTop: Int,
            @RequestParam("padding_right") paddingRight: Int,
            @RequestParam("padding_bottom") paddingBottom: Int,
            @RequestParam("padding_left") paddingLeft: Int
    ): ResponseEntity<StreamingResponseBody> {

        val uuid = UUID.randomUUID()
        val filename = "${uuid}.pdf"
        val path = "./shrinked/${filename}"

        val newFile = pdfService.crop(multipartFile.inputStream, paddingLeft, paddingTop, paddingRight, paddingBottom)

        newFile.save(path)
        newFile.close()

        val inputStream: InputStream = FileInputStream(File(path))

        val responseBody = StreamingResponseBody { outputStream: OutputStream ->
            var numberOfBytesToWrite: Int
            val data = ByteArray(1024)
            while (inputStream.read(data, 0, data.size).also { numberOfBytesToWrite = it } != -1) {
                outputStream.write(data, 0, numberOfBytesToWrite)
            }
            inputStream.close()
        }

        File(path).delete()

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=${filename}")
                .contentType(MediaType.APPLICATION_PDF)
                .body(responseBody)
    }
}


interface PdfService {
    fun crop(file: InputStream, paddingLeft: Int, paddingTop: Int, paddingRight: Int, paddingBottom: Int): PDDocument


    fun echo(): String
}

@Service
class PdfServiceImpl() : PdfService {
    override fun crop(file: InputStream, paddingLeft: Int, paddingTop: Int, paddingRight: Int, paddingBottom: Int): PDDocument {
        val pdfDocument = PDDocument.load(file)

        pdfDocument.pages.forEach {
            val rectangle = it.cropBox
            rectangle.upperRightY -= paddingTop
            rectangle.upperRightX -= paddingRight
            rectangle.lowerLeftY += paddingBottom
            rectangle.lowerLeftX += paddingLeft
            it.cropBox = rectangle
        }
        return pdfDocument
    }

    override fun echo(): String {
        return "EEEOOOOO"
    }


}

