# /bin/bash
export DOCKER_BUILDKIT=1
docker build -t trimminger ./
set DATETIME (date +%Y%m%d_%H%M%S)
echo $DATETIME
docker tag trimminger:latest registry.gitlab.com/morifuji/k8lab.js/trimminger:$DATETIME
docker push registry.gitlab.com/morifuji/k8lab.js/trimminger:$DATETIME
echo $DATETIME

# yaml置換
kubectl set image -f ./k8s/deployment.yml trimminger-app=registry.gitlab.com/morifuji/k8lab.js/trimminger:$DATETIME --local -o yaml > ./k8s/deployment_.yml
rm -rf ./k8s/deployment.yml
mv ./k8s/deployment_.yml ./k8s/deployment.yml

# サーバーに適用
k apply -f ./k8s/deployment.yml
