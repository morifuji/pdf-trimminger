import React, { useState, useEffect, ChangeEvent } from "react";
import "./App.css";
import PDFJS, { PDFDocumentProxy } from "pdfjs-dist";
import axios from "axios";

const bulmaSlider = require("bulma-slider");
const pdfjsWorker = require("pdfjs-dist/build/pdf.worker.entry");

PDFJS.GlobalWorkerOptions.workerSrc = pdfjsWorker;

function App() {
  const [lowerLeft, setLowerLeft] = useState([0, 0]);
  const [upperRight, setUpperRight] = useState([0, 0]);

  const [pdfLowerLeft, setPdfLowerLeft] = useState([0, 0]);
  const [pdfUpperRight, setPdfUpperRight] = useState([0, 0]);

  const [scale, setScale] = useState(1);

  const [canvasWidth, setCanvasWidth] = useState(1);

  const [pageNum, setPageNum] = useState(1);

  const [pdf, setPdf] = useState<File | null>(null);
  const [pdfTask, setPdfTask] = useState<PDFDocumentProxy | null>(null);

  const [errorMessageList, setErrorMessageList] = useState<string[]>([]);

  const [isLoading, setIsLoading] = useState(false);

  const onChangeFile = (event: ChangeEvent<HTMLInputElement>) => {
    if (
      event === null ||
      event.target === null ||
      event.target.files === null ||
      event.target.files.length === 0
    ) {
      return;
    }

    var file = event.target.files[0];
    setPdf(file);

    var fileReader = new FileReader();

    fileReader.onload = (e) => {
      const result = e.target?.result;
      if (!result || typeof result === "string") {
        return;
      }

      var typedarray = new Uint8Array(result);
      const task = PDFJS.getDocument(typedarray);
      task.promise
        .then((pdf) => {
          setPdfTask(pdf);
        })
        // @ts-ignore
        .catch((err) => {
          setErrorMessageList(["PDFとして読み込めませんでした。"]);
        });
    };
    fileReader.readAsArrayBuffer(file);
  };

  useEffect(() => {
    if (pdfTask === null || pageNum <= 0) return;

    pdfTask
      .getPage(pageNum)
      .then((page) => {
        const canvas = document.getElementById(
          "the-canvas"
        ) as HTMLCanvasElement;

        const widthScale =
          canvas.width / page.getViewport({ scale: 1.0 }).width;
        setScale(widthScale);

        // // Get viewport (dimensions)
        var widthViewport = page.getViewport({
          scale: widthScale,
        });

        canvas.height = widthViewport.height;

        var context = canvas.getContext("2d")!;

        var renderContext = {
          canvasContext: context,
          viewport: widthViewport,
        };

        setLowerLeft((value) => {
          return value[0] === 0 && value[1] === 0
            ? [page.view[0] + 10, page.view[1] + 10]
            : value;
        });
        setUpperRight((value) => {
          return value[0] === 0 && value[1] === 0
            ? [page.view[2] - 10, page.view[3] - 10]
            : value;
        });
        setPdfLowerLeft([page.view[0], page.view[1]]);
        setPdfUpperRight([page.view[2], page.view[3]]);

        // Render PDF page
        page.render(renderContext);
      })
      // @ts-ignore
      .catch((err) => {
        setErrorMessageList(["存在しないページ数です"]);
      });
  }, [pageNum, pdfTask]);


  useEffect(() => {
    bulmaSlider.attach();
    const canvas = document.getElementById("the-canvas") as HTMLCanvasElement;
    setCanvasWidth(Math.round(canvas.getClientRects()[0].width));
  }, []);

  const cropWidth = upperRight[0] - lowerLeft[0];
  const cropHeight = upperRight[1] - lowerLeft[1];

  const submit = () => {
    const formData = new FormData();
    if (pdf === null) {
      setErrorMessageList(["PDFが選択されていません"])
      return
    }
    formData.append("upload_file", pdf);
    formData.append(
      "padding_top",
      String(Math.round(pdfUpperRight[1] - upperRight[1]))
    );
    formData.append(
      "padding_left",
      String(Math.round(lowerLeft[0] - pdfLowerLeft[0]))
    );
    formData.append(
      "padding_bottom",
      String(Math.round(lowerLeft[1] - pdfLowerLeft[1]))
    );
    formData.append(
      "padding_right",
      String(Math.round(pdfUpperRight[0] - upperRight[0]))
    );
    setIsLoading(true)
    return axios
      .post("/api/trim", formData, {
        headers: {
          "content-type": "multipart/form-data",
        },
        responseType: "blob",
      })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", pdf.name);
        document.body.appendChild(link);
        link.click();
      })
      .catch((err) => {
        setErrorMessageList(["サーバーでの処理が失敗しました. 時間をおいて再度お試しください"])
        console.warn(err);
      }).finally(()=>{
        setIsLoading(false)
      })
  };

  return (
    <div className="container">
      {errorMessageList.length !== 0 && (
        <div className="columns px-6 py-6">
          <div className="notification is-danger" style={{ width: "100%" }}>
            <button
              className="delete"
              onClick={() => setErrorMessageList([])}
            ></button>
            {errorMessageList.map((message) => (
              <p>{message}</p>
            ))}
          </div>
        </div>
      )}
      <div className="columns px-6 py-6">
        <div className="column is-4">
          <div style={{ position: "relative", width: "100%" }}>
            <canvas
              id="the-canvas"
              height="1"
              width={canvasWidth}
              style={{ width: "100%" }}
            ></canvas>
            {pdfTask && (
              <>
                <div
                  style={{
                    position: "absolute",
                    width: cropWidth * scale,
                    height: cropHeight * scale,
                    border: "solid 1px black",
                    top: (pdfUpperRight[1] - upperRight[1]) * scale,
                    left: (lowerLeft[0] - pdfLowerLeft[0]) * scale,
                  }}
                ></div>
                <div
                  style={{
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    border: "solid 1px black",
                    top: 0,
                    left: 0,
                  }}
                ></div>
              </>
            )}
          </div>
          {pdfTask && (
            <div className="px-5 py-5 is-flex">
              <div>
                <button
                  className="button is-success is-outlined"
                  onClick={() => setPageNum(pageNum - 1)}
                >
                  <span className="icon is-small">
                    <i className="fas fa-arrow-left"></i>
                  </span>
                  <span>戻る</span>
                </button>
              </div>
              <div style={{ marginLeft: "auto" }}>
                <button
                  className="button is-success is-outlined"
                  onClick={() => setPageNum(pageNum + 1)}
                >
                  <span>次へ</span>
                  <span className="icon is-small">
                    <i className="fas fa-arrow-right"></i>
                  </span>
                </button>
              </div>
            </div>
          )}
        </div>
        <div className="column is-8" style={{ textAlign: "left" }}>
          <div className="field">
            <label className="label">サンプルページ番号</label>
            <div className="control">
              <input
                className="input is-info"
                type="number"
                value={pageNum}
                onChange={(e) => setPageNum(Number(e.target.value))}
                placeholder="サンプルページ番号"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">PDFファイル</label>
            <div className="field">
              <div className="file">
                <label className="file-label">
                  <input
                    className="file-input"
                    id="file"
                    type="file"
                    name="resume"
                    onChange={(e) => onChangeFile(e)}
                  />
                  <span className="file-cta">
                    <span className="file-icon">
                      <i className="fas fa-upload"></i>
                    </span>
                    <span className="file-label">Choose a file…</span>
                  </span>
                </label>
              </div>
            </div>
          </div>
          <div className="field">
            <label className="label">左端</label>
            <div className="field">
              <input
                className="slider is-fullwidth"
                step="1"
                min={pdfLowerLeft[0]}
                max={pdfUpperRight[0]}
                value={lowerLeft[0]}
                onChange={(e) =>
                  setLowerLeft([Number(e.target.value), lowerLeft[1]])
                }
                type="range"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">右端</label>
            <div className="field">
              <input
                className="slider is-fullwidth"
                step="1"
                min={pdfLowerLeft[0]}
                max={pdfUpperRight[0]}
                value={upperRight[0]}
                onChange={(e) =>
                  setUpperRight([Number(e.target.value), upperRight[1]])
                }
                type="range"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">上端</label>
            <div className="field">
              <input
                className="slider is-fullwidth"
                step="1"
                min={pdfLowerLeft[1]}
                max={pdfUpperRight[1]}
                value={upperRight[1]}
                onChange={(e) =>
                  setUpperRight([upperRight[0], Number(e.target.value)])
                }
                type="range"
              />
            </div>
          </div>
          <div className="field">
            <label className="label">下端</label>
            <div className="field">
              <input
                className="slider is-fullwidth"
                step="1"
                min={pdfLowerLeft[1]}
                max={pdfUpperRight[1]}
                value={lowerLeft[1]}
                onChange={(e) =>
                  setLowerLeft([lowerLeft[0], Number(e.target.value)])
                }
                type="range"
              />
            </div>
          </div>

          <div className="field is-grouped">
            <div className="control">
              <button className={"button is-link " + (isLoading ? "is-loading" : "")} disabled={isLoading} onClick={submit}>
                切り取ってダウンロード
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
