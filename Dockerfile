# syntax=docker/dockerfile:experimental
FROM openjdk:14-jdk AS build
WORKDIR /workspace/app
COPY . /workspace/app
RUN --mount=type=cache,target=/root/.gradle ./gradlew clean build

FROM openjdk:14-jdk
RUN mkdir /shrinked
COPY --from=build /workspace/app/build/libs/trimminger-0.0.1-SNAPSHOT.jar trimminger-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar", "trimminger-0.0.1-SNAPSHOT.jar"]
